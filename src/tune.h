/*
 * (c) Thomas Pornin 1999 - 2002
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. The name of the authors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef UCPP__TUNE__
#define UCPP__TUNE__
#include "ucpp-config.h"

/* ====================================================================== */
/*
 * Some constants used for memory increment granularity. Increasing these
 * values reduces the number of calls to malloc() but increases memory
 * consumption.
 *
 * Values should be powers of 2.
 */

/* for cpp.c */
#define COPY_LINE_LENGTH	80
#define INPUT_BUF_MEMG		8192
#define OUTPUT_BUF_MEMG		8192
#define TOKEN_NAME_MEMG		64	/* must be at least 4 */
#define TOKEN_LIST_MEMG		32
#define INCPATH_MEMG		16
#define GARBAGE_LIST_MEMG	32
#define LS_STACK_MEMG		4
#define FNAME_MEMG		32

/* ====================================================================== */

/* To protect the innocent. */
#if defined(NO_UCPP_BUF) && defined(UCPP_MMAP)
#undef UCPP_MMAP
#endif

#if defined(UCPP_MMAP) || defined(POSIX_JMP)
#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE	1
#endif
#endif

/*
 * C90 does not know about the "inline" keyword, but C99 does know,
 * and some C90 compilers know it as an extension. This part detects
 * these occurrences.
 */

#ifndef INLINE

#if __STDC__ && __STDC_VERSION__ >= 199901L
/* this is a C99 compiler, keep inline unchanged */
#elif defined(__GNUC__)
/* this is GNU gcc; modify inline. The semantics is not identical to C99
   but the differences are irrelevant as long as inline functions are static */
#undef inline
#define inline __inline__
#elif defined(__DECC) && defined(__linux__)
/* this is Compaq C under Linux, use __inline__ */
#undef inline
#define inline __inline__
#else
/* unknown compiler -> deactivate inline */
#undef inline
#define inline
#endif

#else
/* INLINE has been set, use its value */
#undef inline
#define inline INLINE
#endif

#endif
