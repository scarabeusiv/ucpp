ucpp
====

A C preprocessor compliant to ISO-C99.

Autotools based version with few cleanups, upstream at http://code.google.com/p/ucpp/.

This version is based of 1.3.2 as upstream git repository is broken.
